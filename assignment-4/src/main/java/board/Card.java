package board;

import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Card extends JButton {

    private final String hiddenIconPath =
            "img\\HiddenCard.jpg";
    private final Dimension dimension =
            new Dimension(100, 100);

    private ImageIcon cardImage;
    private ImageIcon hiddenCardImage;
    private int cardId;
    private boolean chosen = false;

    /**
     * Constructor for card.
     * Construct card with id, card image, hidden card image,
     * then set card icon and card dimension.
     *
     * @param cardId id of the card
     */
    public Card(int cardId) {
        this.cardId = cardId;
        this.cardImage = new ImageIcon(String.format("img\\img-%s.jpg", cardId));
        this.hiddenCardImage = new ImageIcon(hiddenIconPath);
        this.setIcon(cardImage);
        this.setPreferredSize(dimension);
    }

    /**
     * Getter method for boolean chosen.
     *
     * @return true if the card has been chosen
     */
    public boolean isChosen() {
        return chosen;
    }

    /**
     * Method for comparing equality of cards.
     * Compare the id of the card to another card.
     *
     * @param otherCard the card that want to be compared with
     *                  current card
     * @return true if the id is equal to each other
     */
    public boolean equals(Card otherCard) {
        return this.cardId == otherCard.cardId;
    }

    /**
     * Method for showing the card.
     * Set the card icon to cardImage
     */
    public void showCard() {
        this.setIcon(cardImage);
    }

    /**
     * Method for hiding the card.
     * Set the card icon to hiddenCardImage
     */
    public void hideCard() {
        this.setIcon(hiddenCardImage);
    }

    /**
     * Setter method for boolean chosen.
     *
     * @param chosen the condition of card whether
     *               has been chosen or not
     */
    public void setChosen(boolean chosen) {
        this.chosen = chosen;
    }
}
