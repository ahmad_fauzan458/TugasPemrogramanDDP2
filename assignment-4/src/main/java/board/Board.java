package board;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Board extends JPanel {
    private final int NUMBER_OF_CARD = 36;
    private final int MIN_CARD_ID = 1;
    private final int MAX_CARD_ID = 18;
    private final int CARD_PAIR = 2;
    private final int BOARD_ROW = 6;
    private final int BOARD_COLUMN = 6;
    private final int MAX_SELECTED_CARDS = 2;

    private ActionListener cardAction;
    private Timer actionTimer = null;

    private ArrayList<Card> selectedCards
            = new ArrayList<>();
    private ArrayList<Card> cards =
            new ArrayList<>();

    private int numberOfCardMatchSuccess = 0;
    private int numberOfCardMatchTry = 0;
    private boolean cardListenerActivated = false;

    /**
     * Constructor for Board.
     * When board has constructed, it consist of cards
     * that set to passive until the board is activated
     */
    public Board() {
        this.setLayout(new GridLayout(BOARD_ROW, BOARD_COLUMN));
        this.setBackground(new Color(109, 133, 255));
        initCardAction();
        generateCardsId();
        generateCards();
        placeCard();
    }

    /**
     * Initiate card action for every card in cards.
     * There are some error handling using guard clause,
     * when card listener not activated yet, and when card
     * already chosen.
     */
    private void initCardAction() {
        this.cardAction = e -> {
            //The action won't be executed until the board activated
            if (!cardListenerActivated) {
                return;
            }

            Object object = e.getSource();
            Card card = (Card) (object);

            //The action won't be executed if card already chosen
            if (card.isChosen()) {
                return;
            }

            //match selected cards will run without delay when action timer already running
            if (actionTimer != null && actionTimer.isRunning()) {
                actionTimer.stop();
                matchSelectedCards();
            }

            chooseCard(card);

            if (selectedCards.size() == MAX_SELECTED_CARDS) {
                delayedMatchSelectedCards();
            }
        };
    }

    /**
     * Generate id for every card in cards.
     *
     * @return ArrayList of Integer Id
     */
    private ArrayList<Integer> generateCardsId() {
        ArrayList<Integer> cardIdArray = new ArrayList<>();
        for (int i = 0; i < CARD_PAIR; i++) {
            for (int j = MIN_CARD_ID; j <= MAX_CARD_ID; j++) {
                cardIdArray.add(j);
            }
        }
        Collections.shuffle(cardIdArray);
        return cardIdArray;
    }

    /**
     * Generate card using the card id that has been generated,
     * then store it in cards. Also, set the action listener.
     */
    private void generateCards() {
        Card card;
        ArrayList<Integer> randomedCardIdArray = generateCardsId();
        int idIndex = 0;
        for (int i = 0; i < NUMBER_OF_CARD; i++) {
            card = new Card(randomedCardIdArray.get(idIndex));
            idIndex++;
            card.addActionListener(cardAction);
            cards.add(card);
        }
    }

    /**
     * Place every card in cards to board.
     */
    private void placeCard() {
        for (Card card : cards) {
            this.add(card);
        }
    }

    /**
     * Activate the board, after the board is activated,
     * the board can be used properly.
     * Activating steps consist of hide card and
     * activate card listener.
     */
    public void activateBoard() {
        Action action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                hideCards();
                activateCardListener();
            }
        };
        Timer peekTimer = new Timer(4000, action);
        peekTimer.setRepeats(false);
        peekTimer.start();
    }

    /**
     * Activate card listener, so card can be used properly.
     */
    private void activateCardListener() {
        cardListenerActivated = true;
    }

    /**
     * Choose a specific card.
     * The card that has been chosen would run some
     * statements then add to selected cards.
     *
     * @param card card that want to be chosen
     */
    private void chooseCard(Card card) {
        card.setChosen(true);
        card.showCard();
        selectedCards.add(card);
    }

    /**
     * Hide every card in cards.
     */
    private void hideCards() {
        for (Card card : cards) {
            card.hideCard();
        }
    }

    /**
     * Eliminate a specific card.
     *
     * @param card card that want to be eliminated
     */
    private void eliminateCard(Card card) {
        card.setVisible(false);
    }

    /**
     * Eliminate cards in selectedCards array.
     */
    private void eliminateSelectedCards() {
        for (Card card : selectedCards) {
            eliminateCard(card);
        }
    }

    /**
     * reset cards in selectedCards.
     * card that has been reset will be unchosen
     * and hidden.
     * Then, clear the selectedCards arraylist.
     */
    private void resetSelectedCards() {
        for (Card card : selectedCards) {
            card.setChosen(false);
            card.hideCard();
        }
        selectedCards.clear();
    }

    /**
     * Match cards in selected cards array.
     * Increment match try.
     * If cards are equal, increment match succes.
     * The selectedCards will be reseted after matching.
     */
    private void matchSelectedCards() {
        if (isSelectedCardsEquals()) {
            numberOfCardMatchSuccess++;
            numberOfCardMatchTry++;
            eliminateSelectedCards();
            resetSelectedCards();

        } else {
            numberOfCardMatchTry++;
            resetSelectedCards();
        }
    }

    /**
     * Run match selected cards using delay.
     */
    private void delayedMatchSelectedCards() {
        Action action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                matchSelectedCards();
            }
        };
        actionTimer = new Timer(500, action);
        actionTimer.setRepeats(false);
        actionTimer.start();
    }

    /**
     * Check whether the board is solved or not.
     * board is solved when the statements is fulfilled.
     *
     * @return true if board has been solved
     */
    public boolean isSolved() {
        return numberOfCardMatchSuccess == NUMBER_OF_CARD / CARD_PAIR;
    }

    /**
     * Check whether selected cards is equal or not.
     *
     * @return true if selected cards is equal
     */
    private boolean isSelectedCardsEquals() {
        boolean result = true;
        for (int i = 0; i < selectedCards.size() - 1; i++) {
            result &= selectedCards.get(i).equals(selectedCards.get(i + 1));
        }
        return result;
    }


    /**
     * Getter method for numberOfCardMatchTry.
     *
     * @return the number of card match try
     */
    public int getNumberOfCardMatchTry() {
        return numberOfCardMatchTry;
    }

    /**
     * Getter method for cards.
     *
     * @return cards in the board
     */
    public ArrayList<Card> getCards() {
        return new ArrayList<>(cards);
    }


}
