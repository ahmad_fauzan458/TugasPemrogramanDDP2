import board.Board;
import board.Card;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.GridLayout;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class MatchPairGame extends JFrame {
    private final String winUrl =
            "html file\\Congratulations.html";

    private JPanel mainPanel;
    private Board board;
    private JLabel numberOfTriesLabel;
    private boolean win = false;

    /**
     * Constructor of Match Pair Game.
     * Init the feature and set the frame condition.
     */
    public MatchPairGame() {
        initMainPanel();
        initMenu();
        initBoard();
        initPropertyChangeListener();
        initFooter();
        this.setTitle("Find the Mythical Creatures");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().add(mainPanel);
        this.pack();
        this.setVisible(true);
        this.setResizable(false);
    }

    /**
     * Initiate main panel for match pair game
     * using border layout.
     */
    private void initMainPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
    }

    /**
     * Init board for match pair game, add the
     * board to main panel.
     */
    private void initBoard() {
        board = new Board();
        mainPanel.add(board, BorderLayout.PAGE_START);
        board.activateBoard();
    }

    /**
     * Init menu consist of restart button and exit button.
     * Add the menu to main panel.
     */
    private void initMenu() {
        JPanel menuPanel = new JPanel();
        menuPanel.setLayout(new GridLayout(1, 2));
        menuPanel.add(restartButton());
        menuPanel.add(exitButton());
        mainPanel.add(menuPanel, BorderLayout.CENTER);
    }

    /**
     * Construct restart button with its action.
     *
     * @return the restart button
     */
    private JButton restartButton() {
        JButton restartButton = new JButton("Play Again?");
        restartButton.setBackground(new Color(170, 233, 255));
        restartButton.addActionListener(e -> restartGame());
        return restartButton;
    }

    /**
     * Method to restart game.
     * When the game restarted, change the board
     * to new board.
     */
    private void restartGame() {
        mainPanel.remove(board);
        initBoard();
        initPropertyChangeListener();
        this.validate();
        this.repaint();
    }

    /**
     * Construct exitButton with its action.
     *
     * @return the exit button
     */
    private JButton exitButton() {
        JButton exitButton = new JButton("Exit");
        exitButton.setBackground(new Color(141, 242, 138));
        exitButton.addActionListener(e -> this.dispose());
        return exitButton;
    }

    /**
     * Initiate footer.
     * The footer consist of number of try label.
     * add the footer to main panel.
     */
    private void initFooter() {
        numberOfTriesLabel = new JLabel(String.format("Number of tries: %s", board.getNumberOfCardMatchTry()));
        mainPanel.add(numberOfTriesLabel, BorderLayout.PAGE_END);
    }

    /**
     * Initiate property change listener as addition to
     * current card listener.
     */
    private void initPropertyChangeListener() {
        for (Card card : board.getCards()) {
            card.addPropertyChangeListener(numberOfTryListener());
            card.addPropertyChangeListener(winGameListener());
        }
    }

    /**
     * Listener for number of try label.
     * Refresh the current number of try in number of try label.
     *
     * @return the listener
     */
    private PropertyChangeListener numberOfTryListener() {
        PropertyChangeListener action = e -> {
            numberOfTriesLabel.setText
                    (String.format("Number of tries: %s", board.getNumberOfCardMatchTry()));
            winGame();
        };
        return action;
    }

    /**
     * Listener for check whether the user already win or not.
     *
     * @return the listener
     */
    private PropertyChangeListener winGameListener() {
        PropertyChangeListener action = (e -> winGame());
        return action;
    }

    /**
     * Check whether the user already win or not.
     * User counted as win when the board is solved.
     * When the user already win, deactivate the method.
     * Open a html file when the user is win.
     */
    private void winGame() {
        if (board.isSolved() && !win) {
            win = true;
            try {
                File htmlFile = new File(winUrl);
                Desktop.getDesktop().browse(htmlFile.toURI());
            } catch (IOException e) {
                this.dispose();
                e.printStackTrace();
            }
        }
    }

    /**
     * Run the game.
     *
     * @param args run game arguments
     */
    public static void main(String[] args) {
        new MatchPairGame();
    }
}
