package animal;

/**
 * Class Animal merepresentasikan Animal dengan
 * berbagai macam attribut dan method sesuai spesifikasi.
 */
public class Animal {
    protected String species;
    protected String name;
    protected int bodyLength;
    protected boolean wild;

    public Animal(String name, int bodyLength) {
        this.name = name;
        this.bodyLength = bodyLength;
    }

    public boolean isWild() {
        return this.wild;
    }

    public String getName() {
        return this.name;
    }

    public int getBodyLength() {
        return this.bodyLength;
    }

    public String getSpecies() {
        return this.species;
    }

}
