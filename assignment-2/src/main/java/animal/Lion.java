package animal;

/**
 * Class Lion merepresentasikan Animal Lion dengan
 * berbagai macam tindakan yang bisa ia lakukan.
 */
public class Lion extends Animal {

    public Lion(String name, int bodyLength) {
        super(name, bodyLength);
        this.wild = true;
    }

    public String hunt() {
        return "err....";
    }

    public String brushed() {
        return "Hauhhmm!";
    }

    public String disturbed() {
        return "HAUHHMM!";
    }
}
