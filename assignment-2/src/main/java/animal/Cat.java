package animal;

import java.util.Random;

/**
 * Class Cat merepresentasikan Animal Cat dengan
 * berbagai macam tindakan yang bisa ia lakukan.
 */
public class Cat extends Animal {
    private String[] voices = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
    private static Random random = new Random();

    public Cat(String name, int bodyLength) {
        super(name, bodyLength);
        this.wild = false;
    }

    public String cuddled() {
        int voiceIndex = random.nextInt(4);
        return this.voices[voiceIndex];
    }

    public String brushed() {
        return "Nyaaan...";
    }

}
