package animal;


/**
 * Class Hamster merepresentasikan Animal Hamster dengan
 * berbagai macam tindakan yang bisa ia lakukan.
 */
public class Hamster extends Animal {
    public Hamster(String name, int bodyLength) {
        super(name, bodyLength);
        this.wild = false;
    }

    public String gnaw() {
        return "ngkkrit.. ngkkrrriiit";
    }

    public String runInsideWheel() {
        return "trrr... trrr...";
    }
}
