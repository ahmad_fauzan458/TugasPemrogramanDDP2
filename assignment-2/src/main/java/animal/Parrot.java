package animal;

/**
 * Class Parrot merepresentasikan Animal Parrot dengan
 * berbagai macam tindakan yang bisa ia lakukan.
 */
public class Parrot extends Animal {

    public Parrot(String name, int bodyLength) {
        super(name, bodyLength);
        this.wild = false;
    }

    public String fly() {
        return "FLYYYY... . .";
    }

    /**
     * Fungsi untuk meniru perkataan manusia.
     * Terdapat kasus khusus jika tidak ada perkataan yang ditiru.
     * @param speech perkataan yang ingin ditiru.
     * @return tiruan dari perkataan.
     */
    public String mimicHumanSpeech(String speech) {
        if (speech.equals("")) {
            return "HM?";
        }
        return speech.toUpperCase();
    }

}
