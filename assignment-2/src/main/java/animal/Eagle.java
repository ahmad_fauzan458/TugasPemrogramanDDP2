package animal;

/**
 * Class Eagle merepresentasikan Animal Eagle dengan
 * berbagai macam tindakan yang bisa ia lakukan.
 */
public class Eagle extends Animal {

    public Eagle(String name, int bodyLength) {
        super(name, bodyLength);
        this.wild = true;
    }

    public String fly() {
        return "kwaakk....";
    }

}
