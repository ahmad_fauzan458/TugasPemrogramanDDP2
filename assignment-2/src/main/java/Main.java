import animal.Animal;
import animal.Cat;
import animal.Eagle;
import animal.Hamster;
import animal.Lion;
import animal.Parrot;
import cage.Cage;
import cage.IndoorCage;
import cage.OutdoorCage;
import java.util.Scanner;
import manager.Manager;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static IndoorCage[][] cats;
    private static OutdoorCage[][] lions;
    private static OutdoorCage[][] eagles;
    private static IndoorCage[][] parrots;
    private static IndoorCage[][] hamsters;

    private static int numberOfCat = 0;
    private static int numberOfLion = 0;
    private static int numberOfEagle = 0;
    private static int numberOfParrot = 0;
    private static int numberOfHamster = 0;

    public static void main(String[] args) {

        System.out.println("Welcome to Javari Park!\n"
                + "Input the number of animals");

        System.out.print("cat: ");
        numberOfCat = Integer.parseInt(scanner.nextLine());
        cats = new IndoorCage[3][numberOfCat / 3 + 1];
        inputAnimalData(numberOfCat, cats, "cat");

        System.out.print("lion: ");
        numberOfLion = Integer.parseInt(scanner.nextLine());
        lions = new OutdoorCage[3][numberOfLion / 3 + 1];
        inputAnimalData(numberOfLion, lions, "lion");

        System.out.print("eagle: ");
        numberOfEagle = Integer.parseInt(scanner.nextLine());
        eagles = new OutdoorCage[3][numberOfEagle / 3 + 1];
        inputAnimalData(numberOfEagle, eagles, "eagle");

        System.out.print("parrot: ");
        numberOfParrot = Integer.parseInt(scanner.nextLine());
        parrots = new IndoorCage[3][numberOfParrot / 3 + 1];
        inputAnimalData(numberOfParrot, parrots, "parrot");

        System.out.print("hamster: ");
        numberOfHamster = Integer.parseInt(scanner.nextLine());
        hamsters = new IndoorCage[3][numberOfHamster / 3 + 1];
        inputAnimalData(numberOfHamster, hamsters, "hamster");

        System.out.println("Animals have been successfully recorded!\n");
        System.out.println("=============================================");

        arrangementInfo(cats);
        arrangementInfo(lions);
        arrangementInfo(parrots);
        arrangementInfo(eagles);
        arrangementInfo(hamsters);

        System.out.printf("NUMBER OF ANIMALS:\ncat:%d\nlion:%d\nparrot:%d\n"
                        + "eagle:%d\nhamster:%d\n", numberOfCat, numberOfLion,
                numberOfParrot, numberOfEagle, numberOfHamster);
        System.out.println("\n=============================================");

        while (true) {
            visitAnimal();
            System.out.println();
        }
    }

    /**
     * Fungsi untuk mengatur input data hewan.
     * Hewan dinisiasi sesuai spesifikasi yang diberikan,
     * kemudian melakukan inisiasi cage untuk setiap hewan yang telah dinisiasi,
     * kemudian diletakkan di multidimension array of cage.
     *
     * @param numberOfAnimals jumlah hewan yang ingin diinisiasi
     * @param multiLevelCage  multidimensi array of cage tempat meletakkan Cage dari hewan.
     * @param animalsSpecies  jenis dari hewan yang ingin dinisiasi.
     */
    private static void inputAnimalData(int numberOfAnimals,
                                        Cage[][] multiLevelCage, String animalsSpecies) {
        if (numberOfAnimals == 0) {
            return;
        }

        Animal animal;
        System.out.println("Provide the information of " + animalsSpecies + "(s):");
        String[] temp = scanner.nextLine().split(",");
        for (int i = 0, j = 0; i < numberOfAnimals; i++) {
            String[] arrayOfData = temp[i].split("\\|");
            switch (animalsSpecies) {
                case "cat":
                    animal = new Cat(arrayOfData[0], Integer.parseInt(arrayOfData[1]));
                    break;
                case "lion":
                    animal = new Lion(arrayOfData[0], Integer.parseInt(arrayOfData[1]));
                    break;
                case "eagle":
                    animal = new Eagle(arrayOfData[0], Integer.parseInt(arrayOfData[1]));
                    break;
                case "parrot":
                    animal = new Parrot(arrayOfData[0], Integer.parseInt(arrayOfData[1]));
                    break;
                default: // case hamster
                    animal = new Hamster(arrayOfData[0], Integer.parseInt(arrayOfData[1]));
            }

            Cage cage;
            if (animal.isWild()) {
                cage = new OutdoorCage(animal);
            } else {
                cage = new IndoorCage(animal);
            }


            int residu = numberOfAnimals % 3;
            int normalSize = (numberOfAnimals / 3 == 0) ? 1 : numberOfAnimals / 3;

            if (i != 0 && i % normalSize == 0) {
                j++;
            }
            if (j < 3) {
                multiLevelCage[j][i % normalSize] = cage;
            } else {
                int residuIndex = normalSize;
                if (normalSize == 1) {
                    residuIndex = 0;
                }

                if (residu == 1) {
                    multiLevelCage[2][residuIndex] = cage;
                } else {
                    multiLevelCage[0][residuIndex] = cage;
                    multiLevelCage[1][residuIndex] = cage;
                }
            }
        }
    }

    /**
     * Fungsi untuk mencetak info dari proses menyusun Cage.
     * Menjalankan fungsi menyusun cage didalamnya.
     *
     * @param cages multidimensi array of cage
     *              yang ingin dicetak info penyusunannya.
     */
    private static void arrangementInfo(Cage[][] cages) {
        if (cages[0][0] == null) {
            return;
        }

        System.out.println("Cage arrangement:");
        System.out.print("location: ");
        System.out.println(cages[0][0].getCageLocation());

        printMultiLevelCages(cages);

        System.out.println();
        System.out.println("After rearrangement...");

        Manager.arrangeCageLevel(cages);
        printMultiLevelCages(cages);
        System.out.println();

    }

    /**
     * Fungsi untuk mencetak multilevel Cage sesuai dengan spesifikasi.
     *
     * @param cages multidimensi array of cage yang akan di cetak.
     */
    private static void printMultiLevelCages(Cage[][] cages) {
        for (int i = cages.length - 1; i >= 0; i--) {
            System.out.printf("Level %d: ", (i + 1));
            if (cages[i] == null) {
                continue;
            }
            for (int j = 0; j < cages[i].length; j++) {
                if (cages[i][j] != null) {
                    System.out.printf("%s (%d - %s),",
                            cages[i][j].getAnimal().getName(),
                            cages[i][j].getAnimal().getBodyLength(),
                            cages[i][j].getCageType());
                }
            }
            System.out.println();
        }
    }

    /**
     * Fungsi untuk mengunjungi animal.
     * Animal yang bisa dikunjungi sesuai dengan spesifikasi yang diberikan.
     * Terdapat pilihan Exit untuk keluar dari program.
     */
    public static void visitAnimal() {
        System.out.println("Which animal you want to visit?");
        System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
        String choice = scanner.nextLine();
        switch (choice) {
            case "1":
                visitCat();
                break;
            case "2":
                visitEagle();
                break;
            case "3":
                visitHamster();
                break;
            case "4":
                visitParrot();
                break;
            case "5":
                visitLion();
                break;
            default:
                System.exit(0);
        }
    }

    /**
     * Fungsi untuk mencari animal dengan menggunakan namanya.
     *
     * @param animalName nama dari hewan yang bersangkutan
     * @param animals    multidimensi array of animal
     * @return Animal yang dicari
     */
    private static Animal findAnimal(String animalName, Cage[][] animals) {
        Animal animal = null;
        int i = 0;
        while (animal == null && i < animals.length) {
            int j = 0;
            while (animal == null && j < animals[i].length) {
                if (animals[i][j] != null
                        && animals[i][j].getAnimal().getName().equals(animalName)) {
                    animal = animals[i][j].getAnimal();
                }
                j++;
            }
            i++;
        }

        return animal;
    }

    /**
     * Fungsi untuk menjalankan program saat mengunjungi Cat.
     */
    private static void visitCat() {
        System.out.print("Mention the name of cat you want to visit: ");
        String catName = scanner.nextLine();

        Cat cat = (Cat) findAnimal(catName, cats);

        if (cat == null) {
            System.out.println("There is no cat with that name! Back to the office!");
        } else {
            System.out.printf("You are visiting %s (cat) now, what would you like to do?\n"
                    + "1: Brush the fur 2: Cuddle\n", catName);
            String choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    System.out.println("Time to clean Katty's fur");
                    System.out.printf("%s makes a voice: ", catName);
                    System.out.println(cat.brushed());
                    break;
                case "2":
                    System.out.printf("%s makes a voice: ", catName);
                    System.out.println(cat.cuddled());
                    break;
                default:
                    System.out.println("You do nothing...");
            }
            System.out.println("Back to the office!");
        }
    }

    /**
     * Fungsi untuk menjalankan program saat mengunjungi Lion.
     */
    private static void visitLion() {
        System.out.print("Mention the name of lion you want to visit: ");
        String lionName = scanner.nextLine();

        Lion lion = (Lion) findAnimal(lionName, lions);

        if (lion == null) {
            System.out.println("There is no lion with that name! Back to the office!");
        } else {
            System.out.printf("You are visiting %s (lion) now, what would you like to do?\n"
                    + "1: See it hunting 2: Brush the mane 3: Disturb it\n", lionName);
            String choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    System.out.println("Lion is hunting..");
                    System.out.printf("%s makes a voice: ", lionName);
                    System.out.println(lion.hunt());
                    break;
                case "2":
                    System.out.println("Clean the lion's mane..");
                    System.out.printf("%s makes a voice: ", lionName);
                    System.out.println(lion.brushed());
                    break;
                case "3":
                    System.out.printf("%s makes a voice: ", lionName);
                    System.out.println(lion.disturbed());
                    break;
                default:
                    System.out.println("You do nothing...");
            }
            System.out.println("Back to the office!");
        }
    }

    /**
     * Fungsi untuk menjalankan program saat mengunjungi Eagle.
     */
    private static void visitEagle() {
        System.out.print("Mention the name of eagle you want to visit: ");
        String eagleName = scanner.nextLine();

        Eagle eagle = (Eagle) findAnimal(eagleName, eagles);

        if (eagle == null) {
            System.out.println("There is no eagle with that name! Back to the office!");
        } else {
            System.out.printf("You are visiting %s (eagle) now, what would you like to do?\n"
                    + "1: Order to fly\n", eagleName);
            String choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    System.out.printf("%s makes a voice: ", eagleName);
                    System.out.println(eagle.fly());
                    System.out.println("You hurt!");
                    break;
                default:
                    System.out.println("You do nothing...");
            }
            System.out.println("Back to the office!");
        }
    }

    /**
     * Fungsi untuk menjalankan program saat mengunjungi Parrot.
     */
    private static void visitParrot() {
        System.out.print("Mention the name of parrot you want to visit: ");
        String parrotName = scanner.nextLine();

        Parrot parrot = (Parrot) findAnimal(parrotName, parrots);

        if (parrot == null) {
            System.out.println("There is no parrot with that name! Back to the office!");
        } else {
            System.out.printf("You are visiting %s (parrot) now, what would you like to do?\n"
                    + "1: Order to fly 2: Do conversation\n", parrotName);
            String choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    System.out.printf("Parrot %s flies!\n", parrotName);
                    System.out.printf("%s makes a voice :", parrotName);
                    System.out.println(parrot.fly());
                    break;
                case "2":
                    System.out.print("You say: ");
                    String humanSpeech = scanner.nextLine();
                    System.out.print("Greeny says: ");
                    System.out.println(parrot.mimicHumanSpeech(humanSpeech));
                    break;
                default:
                    System.out.println("You do nothing...");
            }
            System.out.println("Back to the office!");
        }
    }

    /**
     * Fungsi untuk menjalankan program saat mengunjungi Hamster.
     */
    private static void visitHamster() {
        System.out.print("Mention the name of hamster you want to visit: ");
        String hamsterName = scanner.nextLine();

        Hamster hamster = (Hamster) findAnimal(hamsterName, hamsters);

        if (hamster == null) {
            System.out.println("There is no hamster with that name! Back to the office!");
        } else {
            System.out.printf("You are visiting %s (hamster) now, what would you like to do?\n"
                    + "1: See it gnawing 2: Order to run in the hamster wheel\n", hamsterName);
            String choice = scanner.nextLine();
            switch (choice) {
                case "1":
                    System.out.printf("%s makes a voice: ", hamsterName);
                    System.out.println(hamster.gnaw());
                    break;
                case "2":
                    System.out.printf("%s makes a voice: ", hamsterName);
                    System.out.println(hamster.runInsideWheel());
                    break;
                default:
                    System.out.println("You do nothing...");
            }
            System.out.println("Back to the office!");
        }
    }
}
