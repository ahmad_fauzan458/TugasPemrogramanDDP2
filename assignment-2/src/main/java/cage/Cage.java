package cage;

import animal.Animal;

/**
 * Class Cage untuk menampung hewan didalamnya.
 */
public class Cage {
    protected String cageType;
    protected String cageLocation;
    protected Animal animal;

    public Cage(Animal animal) {
        this.animal = animal;
    }

    public Animal getAnimal() {
        return animal;
    }

    public String getCageType() {
        return cageType;
    }

    public String getCageLocation() {
        return cageLocation;
    }
}
