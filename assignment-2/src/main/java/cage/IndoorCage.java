package cage;

import animal.Animal;

/**
 * Class IndoorCage untuk menampung hewan jinak didalamnya.
 */
public class IndoorCage extends Cage {
    public IndoorCage(Animal animal) {
        super(animal);
        this.cageLocation = "indoor";
        if (animal.getBodyLength() >= 60) {
            this.cageType = "C";
        } else if (animal.getBodyLength() >= 45) {
            this.cageType = "B";
        } else {
            this.cageType = "A";
        }
    }
}

