package cage;

import animal.Animal;

/**
 * Class OudoorCage untuk menampung hewan buas didalamnya.
 */
public class OutdoorCage extends Cage {
    public OutdoorCage(Animal animal) {
        super(animal);
        this.cageLocation = "outdoor";
        if (animal.getBodyLength() >= 90) {
            this.cageType = "C";
        } else if (animal.getBodyLength() >= 75) {
            this.cageType = "B";
        } else {
            this.cageType = "A";
        }
    }
}
