package manager;

import cage.Cage;

public class Manager {

    /**
     * Fungsi untuk membalik ururtan array.
     * @param multiLevelCages array yang ingin di balik urutannya.
     */
    public static void reverse(Cage[] multiLevelCages) {
        for (int i = 0; i < multiLevelCages.length / 2; i++) {
            Cage temp = multiLevelCages[i];
            multiLevelCages[i] = multiLevelCages[multiLevelCages.length - 1 - i];
            multiLevelCages[multiLevelCages.length - 1 - i] = temp;
        }
    }

    /**
     * Fungsi untuk menyusun multilevel cage.
     * @param multiLevelCages multilevel cage yang ingin disusun.
     */
    public static void arrangeCageLevel(Cage[][] multiLevelCages) {
        Cage[] temp = multiLevelCages[multiLevelCages.length - 1];
        for (int i = multiLevelCages.length - 1; i > 0; i--) {
            reverse(multiLevelCages[i - 1]);
            multiLevelCages[i] = multiLevelCages[i - 1];
        }
        multiLevelCages[0] = temp;
        reverse(multiLevelCages[0]);
    }

}