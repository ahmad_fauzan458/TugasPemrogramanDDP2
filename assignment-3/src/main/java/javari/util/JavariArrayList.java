package javari.util;

import java.util.ArrayList;

/**
 * This class represents the class for Javari Util Array List.
 */
public class JavariArrayList {

    private static final ArrayList<String> ANIMAL_LIST = new ArrayList<>();

    static {
        ANIMAL_LIST.add("Whale");
        ANIMAL_LIST.add("Cat");
        ANIMAL_LIST.add("Hamster");
        ANIMAL_LIST.add("Lion");
        ANIMAL_LIST.add("Eagle");
        ANIMAL_LIST.add("Parrot");
        ANIMAL_LIST.add("Snake");
    }

    private static final ArrayList<String> HAMSTER_ATTRACTIONS = new ArrayList<>();

    static {
        HAMSTER_ATTRACTIONS.add("Dancing Animals");
        HAMSTER_ATTRACTIONS.add("Passionate Coders");
        HAMSTER_ATTRACTIONS.add("Counting Masters");
    }

    private static final ArrayList<String> LION_ATTRACTIONS = new ArrayList<>();

    static {
        LION_ATTRACTIONS.add("Circles of Fires");
    }

    private static final ArrayList<String> WHALE_ATTRACTIONS = new ArrayList<>();

    static {
        WHALE_ATTRACTIONS.add("Circles of Fires");
        WHALE_ATTRACTIONS.add("Counting Masters");
    }

    private static final ArrayList<String> PARROT_ATTRACTIONS = new ArrayList<>();

    static {
        PARROT_ATTRACTIONS.add("Dancing Animals");
        PARROT_ATTRACTIONS.add("Counting Masters");
    }

    private static final ArrayList<String> SNAKE_ATTRACTIONS = new ArrayList<>();

    static {
        SNAKE_ATTRACTIONS.add("Dancing Animals");
        SNAKE_ATTRACTIONS.add("Passionate Coders");
    }

    private static final ArrayList<String> CAT_ATTRACTIONS = new ArrayList<>();

    static {
        CAT_ATTRACTIONS.add("Dancing Animals");
        CAT_ATTRACTIONS.add("Passionate Coders");
    }

    private static final ArrayList<String> EAGLE_ATTRACTIONS = new ArrayList<>();

    static {
        EAGLE_ATTRACTIONS.add("Circles of Fires");
    }

    private static final ArrayList<String> MAMMAL_SPECIAL_STATUS = new ArrayList<>();

    static {
        MAMMAL_SPECIAL_STATUS.add("");
        MAMMAL_SPECIAL_STATUS.add("pregnant");
        MAMMAL_SPECIAL_STATUS.add("not pregnant");
    }

    private static final ArrayList<String> AVES_SPECIAL_STATUS = new ArrayList<>();

    static {
        AVES_SPECIAL_STATUS.add("");
        AVES_SPECIAL_STATUS.add("laying eggs");
        AVES_SPECIAL_STATUS.add("not laying eggs");
    }

    private static final ArrayList<String> REPTILE_SPECIAL_STATUS = new ArrayList<>();

    static {
        REPTILE_SPECIAL_STATUS.add("");
        REPTILE_SPECIAL_STATUS.add("wild");
        REPTILE_SPECIAL_STATUS.add("tame");
    }

    /**
     * Getter method for mammal special status.
     *
     * @return mammal special status
     */
    public static ArrayList<String> getMammalSpecialStatus() {
        return MAMMAL_SPECIAL_STATUS;
    }

    /**
     * Getter method for aves special status.
     *
     * @return aves special status
     */
    public static ArrayList<String> getAvesSpecialStatus() {
        return AVES_SPECIAL_STATUS;
    }

    /**
     * Getter method for reptile special status.
     *
     * @return reptile special status
     */
    public static ArrayList<String> getReptileSpecialStatus() {
        return REPTILE_SPECIAL_STATUS;
    }

    /**
     * Getter method for hamster attractions.
     *
     * @return hamster attractions
     */
    public static ArrayList<String> getHamsterAttractions() {
        return HAMSTER_ATTRACTIONS;
    }

    /**
     * Getter method for lion attractions.
     *
     * @return lion attractions
     */
    public static ArrayList<String> getLionAttractions() {
        return LION_ATTRACTIONS;
    }

    /**
     * Getter method for whale attractions.
     *
     * @return whale attractions
     */
    public static ArrayList<String> getWhaleAttractions() {
        return WHALE_ATTRACTIONS;
    }

    /**
     * Getter method for parrot attractions.
     *
     * @return parrot attractions
     */
    public static ArrayList<String> getParrotAttractions() {
        return PARROT_ATTRACTIONS;
    }

    /**
     * Getter method for snake attractions.
     *
     * @return snake attractions
     */
    public static ArrayList<String> getSnakeAttractions() {
        return SNAKE_ATTRACTIONS;
    }

    /**
     * Getter method for cat attractions.
     *
     * @return cat attractions
     */
    public static ArrayList<String> getCatAttractions() {
        return CAT_ATTRACTIONS;
    }

    /**
     * Getter method for eagle attractions.
     *
     * @return eagle attractions
     */
    public static ArrayList<String> getEagleAttractions() {
        return EAGLE_ATTRACTIONS;
    }

    /**
     * Getter method for animal list.
     *
     * @return animal list
     */
    public static ArrayList<String> getAnimalList() {
        return ANIMAL_LIST;
    }
}
