package javari.util;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class JavariHashMap {
    private static final HashMap<String, String> ANIMAL_CATEGORY = new HashMap<>();

    static {
        ANIMAL_CATEGORY.put("Cat", "mammals");
        ANIMAL_CATEGORY.put("Lion", "mammals");
        ANIMAL_CATEGORY.put("Hamster", "mammals");
        ANIMAL_CATEGORY.put("Whale", "mammals");

        ANIMAL_CATEGORY.put("Eagle", "aves");
        ANIMAL_CATEGORY.put("Parrot", "aves");

        ANIMAL_CATEGORY.put("Snake", "reptiles");
    }

    private static final HashMap<String, ArrayList<String>> SPECIAL_STATUS = new HashMap<>();

    static {
        SPECIAL_STATUS.put("mammals", JavariArrayList.getMammalSpecialStatus());
        SPECIAL_STATUS.put("aves", JavariArrayList.getAvesSpecialStatus());
        SPECIAL_STATUS.put("reptiles", JavariArrayList.getReptileSpecialStatus());
    }

    private static final HashMap<String, String> SECTIONS = new HashMap<>();

    static {
        SECTIONS.put("mammals", "Explore the Mammals");
        SECTIONS.put("aves", "World of Aves");
        SECTIONS.put("reptiles", "Reptillian Kingdom");
    }

    private static final HashMap<String, ArrayList<String>> ATTRACTIONS = new HashMap<>();

    static {
        ATTRACTIONS.put("Lion", JavariArrayList.getLionAttractions());
        ATTRACTIONS.put("Eagle", JavariArrayList.getEagleAttractions());
        ATTRACTIONS.put("Parrot", JavariArrayList.getParrotAttractions());
        ATTRACTIONS.put("Snake", JavariArrayList.getSnakeAttractions());
        ATTRACTIONS.put("Cat", JavariArrayList.getCatAttractions());
        ATTRACTIONS.put("Hamster", JavariArrayList.getHamsterAttractions());
        ATTRACTIONS.put("Whale", JavariArrayList.getWhaleAttractions());
    }

    /**
     * Getter methodd for map (animal type, animal category).
     * @return animal category map
     */
    public static HashMap<String, String> getAnimalCategory() {
        return ANIMAL_CATEGORY;
    }

    /**
     * Getter method for map (animal category, special status).
     * @return special status map
     */
    public static HashMap<String, ArrayList<String>> getSpecialStatus() {
        return SPECIAL_STATUS;
    }

    /**
     * Getter method for map (animal category, section).
     * @return sections map
     */
    public static HashMap<String, String> getSections() {
        return SECTIONS;
    }

    /**
     * Getter method for map (animal, attraction).
     * @return attractions map
     */
    public static HashMap<String, ArrayList<String>> getAttractions() {
        return ATTRACTIONS;
    }

}
