package javari.park;

import java.util.ArrayList;
import java.util.List;
import javari.animal.Animal;

public class VisitorAttraction implements SelectedAttraction {
    private String attractionName;
    private String animalType;
    List<Animal> performers = new ArrayList<>();

    public VisitorAttraction(String attractionName, String animalType,
                              List<Animal> performers) {
        this.attractionName = attractionName;
        this.animalType = animalType;
        this.performers = performers;
    }

    /**
     * Returns the name of attraction.
     *
     * @return name of the attraction
     */
    public String getName() {
        return attractionName;
    }

    /**
     * Returns ths type of animal(s) that performing in this attraction.
     *
     * @return type of the animal
     */
    public String getType() {
        return animalType;
    }

    /**
     * Returns all performers of this attraction.
     *
     * @return all performers
     */
    public List<Animal> getPerformers() {
        return performers;
    }

    /**
     * Adds a new animal into the list of performers.
     *
     * @param performer an instance of animal
     * @return {@code true} if the animal is successfully added into list of
     *         performers, {@code false} otherwise
     */
    public boolean addPerformer(Animal performer) {
        performers.add(performer);
        return true;
    }

}
