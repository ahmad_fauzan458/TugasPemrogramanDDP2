package javari.park;

import java.util.ArrayList;
import java.util.List;

public class VisitorRegistration implements Registration {
    private int registrationId = 1;
    private String visitorName;
    List<SelectedAttraction> selectedAttractions = new ArrayList<>();

    public VisitorRegistration(int registrationId, String visitorName) {
        this.registrationId = registrationId;
        this.visitorName = visitorName;
    }

    /**
     * Returns the unique ID that associated with visitor's registration
     * in watching an attraction.
     *
     * @return id of the visitor
     */
    public int getRegistrationId() {
        return this.registrationId;
    }

    /**
     * Returns the name of visitor that associated with the registration.
     *
     * @return name of the visitor
     */
    public String getVisitorName() {
        return visitorName;
    }

    /**
     * Changes visitor's name in the registration.
     *
     * @param name name of visitor
     */
    public void setVisitorName(String name) {
        this.visitorName = name;
    }

    /**
     * Returns the list of all attractions that will be watched by the
     * visitor.
     *
     * @return list of all attractions
     */
    public List<SelectedAttraction> getSelectedAttractions() {
        return selectedAttractions;
    }

    /**
     * Adds a new attraction that will be watched by the visitor.
     *
     * @param selected the attraction
     * @return {@code true} if the attraction is successfully added into the
     *         list, {@code false} otherwise
     */
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        selectedAttractions.add(selected);
        return true;
    }
}
