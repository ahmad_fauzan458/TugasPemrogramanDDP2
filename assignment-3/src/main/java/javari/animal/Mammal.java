package javari.animal;

/**
 * This class represents common attributes and behaviours found in all mammal
 * in Javari Park.
 */
public class Mammal extends Animal {
    private String pregnant;

    /**
     * Constructs an instance of {@code Mammal}.
     *
     * @param id        unique identifier
     * @param type      type of animal (mammal)
     * @param name      name of mammal
     * @param gender    gender of mammal (male/female)
     * @param length    length of mammal in centimeters
     * @param weight    weight of mammal in kilograms
     * @param condition health condition of the mammal
     * @param pregnant  special condition of the mammal.
     */
    public Mammal(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, String pregnant) {
        super(id, type, name, gender, length, weight, condition);
        this.pregnant = pregnant;
    }

    /**
     * Method to check specific condition of mammal.
     *
     * @return true if not laying eggs, else false.
     */
    protected boolean specificCondition() {
        return !this.pregnant.equals("pregnant");
    }

}
