package javari.animal;

/**
 * This class represents common attributes and behaviours found in all mammal
 * in Javari Park.
 */
public class Reptilian extends Animal {
    private String tame;

    /**
     * Constructs an instance of {@code Reptilian}.
     *
     * @param id        unique identifier
     * @param type      type of animal (reptilian)
     * @param name      name of reptilian
     * @param gender    gender of reptilian (male/female)
     * @param length    length of reptilian in centimeters
     * @param weight    weight of reptilian in kilograms
     * @param condition health condition of the reptilian
     * @param tame      special condition of the reptilian.
     */
    public Reptilian(Integer id, String type, String name, Gender gender, double length,
                 double weight, Condition condition, String tame) {
        super(id, type, name, gender, length, weight, condition);
        this.tame = tame;
    }

    /**
     * Method to check specific condition of Reptilian.
     *
     * @return true if tame, else false.
     */
    protected boolean specificCondition() {
        return this.tame.equals("tame");
    }
}
