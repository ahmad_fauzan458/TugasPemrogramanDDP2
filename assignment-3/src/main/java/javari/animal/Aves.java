package javari.animal;

/**
 * This class represents common attributes and behaviours found in all aves
 * in Javari Park.
 */
public class Aves extends Animal {
    private String layingEggs;

    /**
     * Constructs an instance of {@code Aves}.
     *
     * @param id         unique identifier
     * @param type       type of animal (aves)
     * @param name       name of aves
     * @param gender     gender of aves (male/female)
     * @param length     length of aves in centimeters
     * @param weight     weight of aves in kilograms
     * @param condition  health condition of the aves
     * @param layingEggs special condition of the aves.
     */
    public Aves(Integer id, String type, String name, Gender gender, double length,
                   double weight, Condition condition, String layingEggs) {
        super(id, type, name, gender, length, weight, condition);
        this.layingEggs = layingEggs;
    }

    /**
     * Method to check specific condition of aves.
     *
     * @return true if not laying eggs, else false.
     */
    protected boolean specificCondition() {
        return this.layingEggs.equals("not laying eggs");
    }
}
