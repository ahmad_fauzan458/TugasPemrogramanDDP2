package javari.animal;


/**
 * This class represents common attributes and behaviours found in all lion
 * in Javari Park.
 */
public class Lion extends Mammal {

    /**
     * Constructs an instance of {@code Lion}.
     *
     * @param id        unique identifier
     * @param type      type of animal (mammal)
     * @param name      name of lion
     * @param gender    gender of lion (male/female)
     * @param length    length of lion in centimeters
     * @param weight    weight of lion in kilograms
     * @param condition health condition of the lion
     * @param pregnant  special condition of the lion.
     */
    public Lion(Integer id, String type, String name, Gender gender, double length,
                  double weight, Condition condition, String pregnant) {
        super(id, type, name, gender, length, weight, condition, pregnant);
    }

    /**
     * Method to check specific condition of Lion.
     *
     * @return true if not laying eggs, else false.
     */
    protected boolean specificCondition() {
        return super.specificCondition() || this.getGender().equals(Gender.MALE);
    }
}
