package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import javari.util.JavariHashMap;

/**
 * This class represents the class for reading sections from a text
 * file that contains CSV data.
 */
public class SectionsReader extends CsvReader {

    private int invalidSectionsCount = 0;

    private LinkedHashMap<String, ArrayList<String>> sectionsRegistered
            = new LinkedHashMap<>();

    /**
     * Constructor of SectionsReader.
     *
     * @param file path of the file
     */
    public SectionsReader(Path file) throws IOException {
        super(file);
        this.setRecords();
    }

    /**
     * set the valid records of attractions and count
     * the invalid records.
     */
    public void setRecords() {
        for (String line : this.getLines()) {
            String[] data = line.split(COMMA);

            boolean validSection =  isValidSection(data[0], data[1], data[2]);
            if (validSection) {
                addSection(data[2], data[0]);
            } else {
                this.invalidSectionsCount += 1;
            }
        }
    }

    /**
     * Method for check valid section.
     *
     * @param animal   the animal
     * @param category the category
     * @param section  the section
     * @return true if valid
     */
    private boolean isValidSection(String animal, String category, String section) {
        return JavariHashMap.getAnimalCategory().get(animal).equals(category)
                && JavariHashMap.getSections().get(category).equals(section);
    }

    /**
     * Method for add section and the animal in it.
     *
     * @param section the section
     * @param animal  the animal
     */
    private void addSection(String section, String animal) {
        if (!sectionsRegistered.containsKey(section)) {
            this.sectionsRegistered.put(section, new ArrayList<>());
        }
        this.sectionsRegistered.get(section).add(animal);
    }

    /**
     * Getter method for number of valid records.
     *
     * @return number of valid records
     */
    public long getNumberOfValidRecords() {
        return this.sectionsRegistered.size();
    }

    /**
     * Getter method for number of invalid records.
     *
     * @return number of invalid records
     */
    public long getNumberOfInvalidRecords() {
        return this.invalidSectionsCount;
    }

    /**
     * Getter method for registered sections.
     *
     * @return registered sections Linked Hash Map (sections, animals)
     */
    public LinkedHashMap<String, ArrayList<String>> getSectionsRegistered() {
        return sectionsRegistered;
    }
}