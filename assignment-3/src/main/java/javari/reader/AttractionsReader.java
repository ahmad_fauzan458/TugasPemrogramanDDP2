package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;

import javari.util.JavariHashMap;

/**
 * This class represents the class for reading attractions from a text
 * file that contains CSV data.
 */
public class AttractionsReader extends CsvReader {

    private HashSet<String> validAttractions = new HashSet<>();

    private int invalidAttractionCount = 0;

    private LinkedHashMap<String, ArrayList<String>> attractionsRegistered
            = new LinkedHashMap<>();

    /**
     * attractions reader constructor.
     *
     * @param file path of the file
     */
    public AttractionsReader(Path file) throws IOException {
        super(file);
        this.setRecords();
    }

    /**
     * set the valid records of attractions and count
     * the invalid records.
     */
    public void setRecords() {

        for (String line : this.getLines()) {
            String[] data = line.split(COMMA);
            boolean valid =  false;

            for (String attraction : JavariHashMap.getAttractions().get(data[0])) {
                if (attraction.equals(data[1])) {
                    addAttractions(data[0], data[1]);
                    validAttractions.add(data[1]);
                    valid = true;
                    break;
                }
            }

            if (!valid) {
                System.out.println(line);
                invalidAttractionCount += 1;
            }
        }
    }

    /**
     * Method for add attraction ArrayList of attraction.
     *
     * @param animal     the animal
     * @param attraction the attractioin
     */
    private void addAttractions(String animal, String attraction) {
        if (!attractionsRegistered.containsKey(animal)) {
            this.attractionsRegistered.put(animal, new ArrayList<>());
        }
        this.attractionsRegistered.get(animal).add(attraction);
    }

    /**
     * Getter method for number of invalid records.
     *
     * @return number of invalid records
     */
    public long getNumberOfInvalidRecords() {
        return this.invalidAttractionCount;
    }

    /**
     * Getter method for number of valid records.
     *
     * @return number of valid records
     */
    public long getNumberOfValidRecords() {
        return this.validAttractions.size();
    }

    /**
     * Getter method for map (String animal, ArrayList attractions).
     *
     * @return the map.
     */
    public LinkedHashMap<String, ArrayList<String>> getAttractionsRegistered() {
        return attractionsRegistered;
    }
}
