package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.HashSet;
import javari.util.JavariHashMap;

/**
 * This class represents the class for reading categories from a text
 * file that contains CSV data.
 */
public class CategoriesReader extends CsvReader {

    private HashSet<String> validCategory = new HashSet<>();
    private int invalidCategoryCount = 0;

    /**
     * Constructor of CategoriesReader.
     *
     * @param file path of the file.
     */
    public CategoriesReader(Path file) throws IOException {
        super(file);
        this.setRecords();
    }

    /**
     * set the valid records of attractions and count
     * the invalid records.
     */
    public void setRecords() {
        for (String line : this.getLines()) {
            String[] data = line.split(COMMA);

            boolean validCategory = isValidCategory(data[0], data[1]);

            if (validCategory) {
                this.validCategory.add(data[1]);
            } else {
                this.invalidCategoryCount += 1;
            }
        }
    }

    /**
     * Method for check valid category.
     *
     * @param animal   animal that want to be checked
     * @param category the category that want to be checked
     * @return true if valid.
     */
    private boolean isValidCategory(String animal, String category) {
        return JavariHashMap.getAnimalCategory().get(animal).equals(category);
    }

    /**
     * Getter method for number of valid records.
     *
     * @return number of valid records
     */
    public long getNumberOfValidRecords() {
        return this.validCategory.size();
    }

    /**
     * Getter method for number of invalid records.
     *
     * @return number of invalid records
     */
    public long getNumberOfInvalidRecords() {
        return this.invalidCategoryCount;
    }

    /**
     * Getter method for valid category.
     *
     * @return the valid category.
     */
    public HashSet<String> getValidCategory() {
        return validCategory;
    }
}