package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import javari.animal.Condition;
import javari.animal.Gender;
import javari.util.JavariArrayList;
import javari.util.JavariHashMap;

/**
 * This class represents the class for reading animal records from a text
 * file that contains CSV data.
 */
public class RecordsReader extends CsvReader {

    private ArrayList<String> validRecords = new ArrayList<>();
    private int invalidRecordCount = 0;

    /**
     * Construction of RecordsReader.
     *
     * @param file path of the file
     */
    public RecordsReader(Path file) throws IOException {
        super(file);
        this.setRecords();
    }

    /**
     * set the valid records of attractions and count
     * the invalid records.
     */
    public void setRecords() {
        for (String line : this.getLines()) {
            String[] data = line.split(COMMA);

            if (this.validId(data[0])
                    && this.validAnimal(data[1])
                    && this.validGender(data[3])
                    && this.validBodyLength(data[4])
                    && this.validBodyWeight(data[5])
                    && this.validSpecialStatus(data[1], data[6])
                    && this.validCondition(data[7])) {
                this.validRecords.add(line);
            } else {
                this.invalidRecordCount += 1;
            }
        }
    }

    /**
     * Method for check valid ID.
     *
     * @param idStr the id thant want to be checked.
     * @return true if valid
     */
    private boolean validId(String idStr) {
        boolean valid;
        try {
            int id = Integer.parseInt(idStr);
            valid = true;
        } catch (Exception e) {
            valid = false;
        }
        return valid;
    }

    /**
     * Method for check valid animal.
     *
     * @param animal animal that want to be checked
     * @return true if valid
     */
    private boolean validAnimal(String animal) {
        for (String animalType : JavariArrayList.getAnimalList()) {
            if (animal.equals(animalType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method for check valid gender.
     *
     * @param gender gender that want to be checked
     * @return true if valid
     */
    private boolean validGender(String gender) {
        boolean valid;
        try {
            Gender.parseGender(gender);
            valid = true;
        } catch (Exception e) {
            valid = false;
        }
        return valid;
    }

    /**
     * Method for check valid body length.
     *
     * @param bodyLength bodyLength that want to be checked
     * @return true if valid
     */
    private boolean validBodyLength(String bodyLength) {
        boolean valid = true;
        if (bodyLength.charAt(0) == ('-')) {
            valid = false;
        } else {
            try {
                double validBodyLength = Double.parseDouble(bodyLength);
            } catch (Exception e) {
                valid = false;
            }
        }
        return valid;
    }

    /**
     * Method for check body weight.
     *
     * @param bodyWeight bodyWeight that want to be checked
     * @return true if valid
     */
    private boolean validBodyWeight(String bodyWeight) {
        boolean valid = true;
        if (bodyWeight.charAt(0) == ('-')) {
            valid = false;
        } else {
            try {
                double validBodyWeight = Double.parseDouble(bodyWeight);
            } catch (Exception e) {
                valid = false;
            }
        }
        return valid;
    }

    /**
     * Method for check valid special status.
     *
     * @param specialStatus specialStatus that want to be checked
     * @return true if valid
     */
    private boolean validSpecialStatus(String animalType, String specialStatus) {
        String animalCategory = JavariHashMap.getAnimalCategory().get(animalType);
        for (String validSpecialStatus : JavariHashMap.getSpecialStatus().get(animalCategory)) {
            if (specialStatus.equals(validSpecialStatus)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method for check condition.
     *
     * @param condition condition that want to be checked
     * @return true if valid
     */
    private boolean validCondition(String condition) {
        boolean valid;
        try {
            Condition.parseCondition(condition);
            valid = true;
        } catch (Exception e) {
            valid = false;
        }
        return valid;
    }

    /**
     * Getter method for number of valid records.
     *
     * @return number of valid records
     */
    public long getNumberOfValidRecords() {
        return validRecords.size();
    }

    /**
     * Getter method for number of invalid records.
     *
     * @return number of invalid records
     */
    public long getNumberOfInvalidRecords() {
        return invalidRecordCount;
    }

    /**
     * Getter method for valid records.
     *
     * @return valid records
     */
    public ArrayList<String> getValidRecords() {
        return validRecords;
    }
}
