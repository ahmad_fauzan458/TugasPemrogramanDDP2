import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Condition;
import javari.animal.Gender;
import javari.animal.Lion;
import javari.animal.Mammal;
import javari.animal.Reptilian;
import javari.park.Registration;
import javari.park.SelectedAttraction;
import javari.park.VisitorAttraction;
import javari.park.VisitorRegistration;
import javari.reader.AttractionsReader;
import javari.reader.CategoriesReader;
import javari.reader.RecordsReader;
import javari.reader.SectionsReader;
import javari.util.JavariHashMap;
import javari.writer.RegistrationWriter;



public class Main {

    private static int idCount = 1;
    private static ArrayList<Animal> animals = new ArrayList<>();
    private static AttractionsReader attractionsReader = null;
    private static CategoriesReader categoriesReader = null;
    private static SectionsReader sectionsReader = null;
    private static RecordsReader recordsReader = null;
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data.");

        boolean readFileSuccess = false;

        String path = System.getProperty("user.dir");

        //Read CSV File based on path, reading from default path for the first time.
        while (!readFileSuccess) {
            try {
                attractionsReader =
                        new AttractionsReader(Paths.get(path + "\\animals_attractions.csv"));
                categoriesReader =
                        new CategoriesReader(Paths.get(path + "\\animals_categories.csv"));
                sectionsReader =
                        new SectionsReader(Paths.get(path + "\\animals_categories.csv"));
                recordsReader =
                        new RecordsReader(Paths.get(path + "\\animals_records.csv"));
                readFileSuccess = true;
            } catch (Exception e) {
                System.out.println(" ... File not found or incorrect file!\n");
                System.out.print("Please provide the source data path: ");
                path = scanner.nextLine();
            }
        }

        //Information of the file that has been read.
        System.out.println("\n... Loading... Success... System is populating data...\n");
        System.out.printf("Found %s valid sections and %s invalid sections\n",
                sectionsReader.getNumberOfValidRecords(),
                sectionsReader.getNumberOfInvalidRecords());
        System.out.printf("Found %s valid attractions and %s invalid attractions\n",
                attractionsReader.getNumberOfValidRecords(),
                attractionsReader.getNumberOfInvalidRecords());
        System.out.printf("Found %s valid animal categories and %s invalid animal categories\n",
                categoriesReader.getNumberOfValidRecords(),
                categoriesReader.getNumberOfInvalidRecords());
        System.out.printf("Found %s valid animal records and %s invalid animal records\n",
                recordsReader.getNumberOfValidRecords(),
                recordsReader.getNumberOfInvalidRecords());

        //Introduction to Javari Park Festival
        System.out.println("\nWelcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. "
                + "Type # if you want to return to the previous menu");

        //Instantiate animal, then run the registration section
        instantiateAnimal();
        boolean moreRegistration = true;
        Registration visitorRegistration = new VisitorRegistration(idCount, "");

        while (moreRegistration) {
            boolean finalCheck = false;
            SelectedAttraction visitorAttraction = null;
            while (!finalCheck) {
                ArrayList<Object> registrationInformation = registration();
                String attraction = (String)registrationInformation.get(0);
                String animalType = (String)registrationInformation.get(1);
                ArrayList<Animal> showableAnimal =
                        (ArrayList<Animal>)registrationInformation.get(2);
                String name = getVisitorName();
                visitorAttraction = new VisitorAttraction(attraction, animalType, showableAnimal);
                visitorRegistration.setVisitorName(name);
                finalCheck = finalCheck(name, attraction, animalType, showableAnimal);
            }
            visitorRegistration.addSelectedAttraction(visitorAttraction);
            moreRegistration = moreRegistration();
        }

        //Add path and write registration file to JSON.
        Path outputPath = FileSystems.getDefault().getPath(path);
        boolean canWrite = false;
        while (!canWrite) {
            try {
                RegistrationWriter.writeJson(visitorRegistration, outputPath);
                canWrite = true;
            } catch (Exception e) {
                System.out.println("Failed to write registration");
                System.out.print("Enter new output path: ");
                outputPath = FileSystems.getDefault().getPath(scanner.nextLine());
            }
        }
    }

    /**
     * Method for instantiate Animal.
     * Every animal that recorded will be instantiated
     */
    private static void instantiateAnimal() {
        for (String line : recordsReader.getValidRecords()) {
            String[] record = line.split(",");

            String animalCategory = JavariHashMap.getAnimalCategory().get(record[1]);

            Integer id = Integer.parseInt(record[0]);
            String type = record[1];
            String name = record[2];
            Gender gender = Gender.parseGender(record[3]);
            double length = Double.parseDouble(record[4]);
            double weight = Double.parseDouble(record[5]);
            Condition condition = Condition.parseCondition(record[7]);
            String specialStatus =  record[6];

            Animal animal;
            switch (animalCategory) {
                case "mammals" :
                    if (type.equals("Lion")) {
                        animal = new Lion(id,type,name,gender,
                                length,weight,condition,specialStatus);
                    } else {
                        animal = new Mammal(id,type,name,gender,length,
                                weight,condition,specialStatus);
                    }
                    break;
                case "aves" :
                    animal = new Aves(id,type,name,gender,length,
                            weight,condition,specialStatus);
                    break;
                default : //case reptiles
                    animal = new Reptilian(id,type,name,gender,length,
                            weight,condition,specialStatus);
            }
            animals.add(animal);
        }
    }

    /**
     * Method for choose sections.
     * Section chosen by user using input.
     *
     * @return the sections chosen.
     */
    private static String chooseSections() {
        System.out.printf("\nJavari park has %s sections:\n",
                sectionsReader.getNumberOfValidRecords());

        String[] sections = sectionsReader.getSectionsRegistered().keySet()
                .toArray(new String[sectionsReader.getSectionsRegistered().size()]);

        int count = 1;
        for (String section : sections) {
            System.out.printf("%s. %s\n", count, section);
            count++;
        }

        System.out.print("Please choose your preferred section (type the number): ");

        int command = Integer.parseInt(scanner.nextLine());

        return sections[command - 1];
    }

    /**
     * Method for choose preferred animal.
     * preferred animal chosen by user using input.
     *
     * @param section section that contain some animal.
     * @return the preferred animal chosen.
     */
    private static String choosePreferredAnimal(String section) {

        System.out.printf("\n--%s--\n", section);
        ArrayList<String> animalsRegistered =
                sectionsReader.getSectionsRegistered().get(section);

        int count = 1;
        for (String animal : animalsRegistered) {
            System.out.printf("%s. %s\n", count, animal);
            count++;
        }

        System.out.print("Please choose your preferred animals (type the number): ");

        int command = Integer.parseInt(scanner.nextLine());

        return animalsRegistered.get(command - 1);
    }

    /**
     * Method for choose preferred attraction.
     * preferred attraction chosen by user using input.
     *
     * @param animal animal that perform attractioin.
     * @return the preferred attraction chosen.
     */
    private static String choosePreferredAttraction(String animal) {
        System.out.printf("\n---%s---\n", animal);
        System.out.printf("Attractions by %s:\n", animal);
        ArrayList<String> attractionsRegistered =
                attractionsReader.getAttractionsRegistered().get(animal);

        int count = 1;
        for (String attraction : attractionsRegistered) {
            System.out.printf("%s. %s\n", count, attraction);
            count++;
        }

        System.out.print("Please choose your preferred attractions (type the number): ");

        int command = Integer.parseInt(scanner.nextLine());

        return attractionsRegistered.get(command - 1);
    }

    /**
     * Method for asking visitor name.
     *
     * @return visitor name.
     */
    private static String getVisitorName() {
        System.out.println("\nWow, one more step,");
        System.out.printf("please let us know your name: ");
        String name = scanner.nextLine();
        return name;
    }

    /**
     * Method for making list of showable animal.
     *
     * @param animalType type of animal that want to be listed.
     * @return showable preferred animals
     */
    private static ArrayList<Animal> showAblePreferredAnimal(String animalType) {
        ArrayList<Animal> showableAnimal = new ArrayList<>();
        for (Animal animal : animals) {
            if (animal.getType().equals(animalType) && animal.isShowable()) {
                showableAnimal.add(animal);
            }
        }
        return showableAnimal;
    }

    /**
     * Method for finalize the registration.
     *
     * @param name           the name of visitor
     * @param attraction     the attraction chosen
     * @param animal         the animal chosen
     * @param showableAnimal the showableAnimal chosen
     * @return true if it's final
     */
    private static boolean finalCheck(String name, String attraction,
                                      String animal, ArrayList<Animal> showableAnimal) {
        System.out.println("\nYeay, final check!");
        System.out.println("Here is your data, and the attraction you chose:");
        System.out.printf("Name: %s\n", name);
        System.out.printf("Attractions: %s -> %s\n", attraction, animal);
        System.out.print("With: ");
        for (int i = 0; i < showableAnimal.size(); i++) {
            String animalName = showableAnimal.get(i).getName();
            System.out.print(animalName.substring(0,1)
                    .toUpperCase() + animalName.substring(1));
            if (i != showableAnimal.size() - 1) {
                System.out.print(", ");
            }
        }
        System.out.println();

        boolean validAnswer = false;
        boolean finalCheck = false;

        while (!validAnswer) {
            System.out.print("\nIs the data correct? (Y/N): ");
            String answer = scanner.nextLine().toUpperCase();
            System.out.println();

            switch (answer) {
                case "Y":
                    finalCheck = true;
                    validAnswer = true;
                    break;
                case "N":
                    finalCheck = false;
                    validAnswer = true;
                    break;
                default :
                    System.out.println("Invalid Input");
            }
        }
        return finalCheck;
    }

    /**
     * Method for perform registration.
     *
     * @return ArrayList (attraction, animalType, showable Animal).
     */
    public static ArrayList<Object> registration() {
        ArrayList<Object> output = new ArrayList<>();

        String section = null;
        String animalType = null;
        ArrayList<Animal> showableAnimal = null;
        String attraction = null;

        int menu = 0;
        boolean breakCase;

        while (menu != 3) {
            switch (menu) {

                case 0 :
                    section = null;
                    while (section == null) {
                        try {
                            section = chooseSections();
                        } catch (Exception e) {
                            section = null;
                        }
                    }
                    menu = 1;
                    break;

                case 1 :
                    showableAnimal = null;
                    breakCase = false;
                    while (showableAnimal == null || showableAnimal.size() == 0) {
                        try {
                            animalType = choosePreferredAnimal(section);
                        } catch (Exception e) {
                            menu = 0;
                            breakCase = true;
                            break;
                        }

                        showableAnimal = showAblePreferredAnimal(animalType);
                        if (showableAnimal.size() == 0) {
                            System.out.printf("\nUnfortunately, no %s can perform "
                                    + "any attraction, please choose other animals\n", animalType);
                        }
                    }

                    if (breakCase) {
                        break;
                    }

                    menu = 2;
                    break;

                case 2 :
                    try {
                        attraction = choosePreferredAttraction(animalType);
                        menu = 3;
                    } catch (Exception e) {
                        menu = 1;
                    }
                    break;

                default :

            }
        }
        output.add(attraction);
        output.add(animalType);
        output.add(showableAnimal);

        return output;
    }

    /**
     * Method for check the next registration.
     *
     * @return true if there will be next registration
     */
    public static boolean moreRegistration() {
        boolean validInput = false;
        boolean moreRegistration = false;
        while (!validInput) {
            System.out.print("Thank you for your interest."
                    + " Would you like to register to other attractions? (Y/N): ");
            String command = scanner.nextLine();
            System.out.println();
            switch (command.toUpperCase()) {
                case "Y":
                    moreRegistration = true;
                    validInput = true;
                    break;
                case "N":
                    moreRegistration = false;
                    validInput = true;
                    break;
                default:
                    System.out.println("Invalid Input");
            }
        }
        return moreRegistration;
    }
}

