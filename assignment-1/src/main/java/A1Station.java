import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    private static int numberOfCarInTrack = 0;
    private static TrainCar mostRecentCar;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfCats = Integer.parseInt(scanner.nextLine()); //scan input for number of cats

        /*
        Instantiate WildCat using input as parameter
        Then, instantiate TrainCar using the WildCat as parameter
        Add the number of TrainCar in track (numberOfCarInTrack) by 1 after instantiate a TrainCar
        if the TrainCar is not first TrainCar in the track, Link the TrainCar to previous TrainCar
        Depart train when the total weight of the train has exceeded the threshold
        or when there is no further input and there is a train on the track
        If the train has departed, empty the track, reset the number of TrainCar in track
        */

        for (int i = 0; i < numberOfCats; i++) {
            String[] catSpesification = scanner.nextLine().split(",");
            String catName = catSpesification[0];
            double catWeight = Double.parseDouble(catSpesification[1]);
            double catLength = Double.parseDouble(catSpesification[2]);
            TrainCar trainCar = new TrainCar(new WildCat(catName, catWeight, catLength));

            numberOfCarInTrack++;
            link(mostRecentCar, trainCar);
            mostRecentCar = trainCar;

            if (trainCar.computeTotalWeight() >= 250) {
                departTrain(trainCar, numberOfCarInTrack);
                mostRecentCar = null;
                numberOfCarInTrack = 0;
            }
        }

        if (mostRecentCar != null) {
            departTrain(mostRecentCar, numberOfCarInTrack);
            mostRecentCar = null;
            numberOfCarInTrack = 0;
        }

        scanner.close();
    }

    /**
     * If there is a previous car, link current car to the previous car.
     * @param mostRecentCar previous TrainCar on the track
     * @param currentCar current TrainCar
     */
    public static void link(TrainCar mostRecentCar, TrainCar currentCar) {
        if (mostRecentCar != null) {
            currentCar.setNext(mostRecentCar);
        }
    }

    /**
     * Depart train, print cats in the train, average mass index, and cats category.
     * @param trainCar the most recent car from the train that want to be departed
     * @param numberOfCarInTrack number of TrainCar in track
     */
    public static void departTrain(TrainCar trainCar, int numberOfCarInTrack) {
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<--");
        trainCar.printCar();
        double averageMassIndex = (trainCar.computeTotalMassIndex() / numberOfCarInTrack);
        System.out.printf("Average mass index of all cats: %.2f\n", averageMassIndex);
        String category = checkCategory(averageMassIndex);
        System.out.printf("In average, the cats in the train are *%s*\n", category);
    }

    /**
     * Check cats category based on average mass index.
     * @param averageMassIndex average mass index that want to be checked
     * @return cats category based on average mass index
     */
    public static String checkCategory(double averageMassIndex) {
        String category;
        if (averageMassIndex >= 30) {
            category = "obese";
        } else if (averageMassIndex >= 25) {
            category = "overweight";
        } else if (averageMassIndex >= 18.5) {
            category = "normal";
        } else {
            category = "underweight";
        }
        return category;
    }
}
