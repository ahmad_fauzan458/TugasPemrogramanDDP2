public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    /**
     * computes the Body Mass Index (BMI) based on cat's weight and length.
     * BMI = weight (kg) / length (m) squared.
     * @return the BMI of cat
     */
    public double computeMassIndex() {
        double lengthInMeter = length * 0.01;
        return weight / (lengthInMeter * lengthInMeter);
    }
}
