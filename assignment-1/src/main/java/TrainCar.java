public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    private WildCat cat;
    private TrainCar next;

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next  = next;
    }

    public WildCat getCat() {
        return cat;
    }

    public void setCat(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar getNext() {
        return next;
    }

    public void setNext(TrainCar next) {
        this.next = next;
    }

    /**
     * recursively calculates the total weight of a car and its content
     * plus total weight from subsequent car(s).
     * @return the total weight of a car and its content plus total weight from subsequent car(s)
     */
    public double computeTotalWeight() {
        if (next == null) {
            return cat.weight + EMPTY_WEIGHT;
        } else {
            return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
        }

    }

    /**
     * calculates the total of body mass index of the cat in this car
     * plus every cat(s) from subsequent car(s).
     * @return the total of body mass index
     */
    public double computeTotalMassIndex() {
        if (next == null) {
            return cat.computeMassIndex();
        } else {
            return cat.computeMassIndex() + next.computeTotalMassIndex();
        }
    }

    /**
     * recursively displays content of this car and the link to subsequent car
     * if this car is not the last car in the train.
     */
    public void printCar() {
        if (next == null) {
            System.out.printf("(%s)\n", cat.name);
        } else {
            System.out.printf("(%s)--", cat.name);
            next.printCar();
        }
    }
}
